const Checkout = require("./Checkout")
const { SinglePriceRule, BundledPriceRule } = require("./PriceRules")
const PriceChart = require("./PriceChart")

const context = describe
const xcontext = xdescribe

describe('Checkout system', () => {
  let checkout

  beforeEach(() => {
    checkout = new Checkout()
  })

  it('scans multiple products', () => {
    checkout.scan('A')
      .scan('B')
      .scan('C')

    expect(checkout.printBasket()).toBe("ABC")
  })

  it('calculates total for single products', () => {
    const productAPrice = 50
    const productBPrice = 30
    const productCPrice = 20
    const productDPrice = 15

    expect(checkout.scan("A").total()).toBe(productAPrice)
    expect(checkout.scan("B").total()).toBe(productBPrice)
    expect(checkout.scan("C").total()).toBe(productCPrice)
    expect(checkout.scan("D").total()).toBe(productDPrice)
  })

  it('calculates total for multiple products', () => {
    const basketOneTotal = 100
    const basketTwoTotal = 115

    expect(checkout.scanMany("AA").total()).toBe(basketOneTotal)
    expect(checkout.scanMany("ABCD").total()).toBe(basketTwoTotal)
  })

  it('applies discounts for bundles', () => {
    const bundleOneTotal = 45
    const bundleTwoTotal = 195

    expect(checkout.scanMany("BB").total()).toBe(bundleOneTotal)
    expect(checkout.scanMany("AAABBC").total()).toBe(bundleTwoTotal)
  })

  context("when recives a custom single prices chart", () => {
    beforeEach(() => {
      const singlePriceChart = new PriceChart({
        A: 10,
        B: 20
      })

      checkout = new Checkout([
        new BundledPriceRule(),
        new SinglePriceRule(singlePriceChart)
      ])
    })

    it('applies the single prices', () => {
      const bundleOneTotal = 20
      const bundleTwoTotal = 65

      expect(checkout.scanMany("AA").total()).toBe(bundleOneTotal)
      expect(checkout.scanMany("BBB").total()).toBe(bundleTwoTotal)
    })
  })

  context("when receives custom bundles price charts", () => {
    beforeEach(() => {
      const bundledPricedChart = new PriceChart({
        A: { quantity: 2, price: 90 },
        B: { quantity: 3, price: 80 }
      })

      checkout = new Checkout([
        new BundledPriceRule(bundledPricedChart),
        new SinglePriceRule() 
      ])
    })

    it('applies the bundles prices', () => {
      const bundleOneTotal = 140
      const bundleTwoTotal = 250

      expect(checkout.scanMany("AAA").total()).toBe(bundleOneTotal)
      expect(checkout.scanMany("BABBBAA").total()).toBe(bundleTwoTotal)
    })
  })
})
