class PriceChart {
  constructor(priceChart) {
    Object.keys(priceChart).forEach((productName) => {
      this[productName] = priceChart[productName]
    })
  }
}

module.exports = PriceChart
