class ProductsCollection {
  constructor() {
    this.products = {}
  }

  getAll() {
    return this.products
  }

  print() {
    const separator = ""

    return Object.keys(this.products).join(separator)
  }

  add(name) {
    if (this.products[name]) {
      this.products[name] += 1
      return
    }

    this.products[name] = 1
  }

  remove(name, quantity) {
    this.products[name] -= quantity
  }
}

module.exports = ProductsCollection
