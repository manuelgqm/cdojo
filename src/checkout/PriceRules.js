class PriceRule {
  constructor(customPriceChart, defaultPriceChart) {
    this.priceChart = customPriceChart || defaultPriceChart 
  }

  apply(basket) {
    console.log("client must implement apply method")
  }
}

class SinglePriceRule extends PriceRule {
  constructor(customPriceChart) {
    const defaultPriceChart = {
      A: 50,
      B: 30, 
      C: 20,
      D: 15
    }

    super(customPriceChart, defaultPriceChart)
  }

  apply(basket) {
    const products = basket.getAll()
    let total = 0

    Object.keys(products).forEach((current) => {
      total += products[current] * this.priceChart[current]

      basket.remove(current, products[current])
    })

    return { basket, total }
  }
}

class BundledPriceRule extends PriceRule{
  constructor(customPriceChart) {
    const defaultPriceChart = {
      A: { quantity: 3, price: 130 },
      B: { quantity: 2, price: 45 }
    }

    super(customPriceChart, defaultPriceChart)
  }

  apply(basket) {
    const products = basket.getAll()
    const bundledProducts = this.getBundledProducts(basket)

    let total = 0
    bundledProducts.forEach((currentProduct) => {
      const productQuantity = products[currentProduct] 
      const bundleQuantity = this.priceChart[currentProduct].quantity
      const bundlePrice = this.priceChart[currentProduct].price
      const bundlesCount = Math.floor(productQuantity / bundleQuantity) 

      total += bundlesCount * bundlePrice 
      basket.remove(currentProduct, bundlesCount * bundleQuantity)
    })

    return { basket, total }
  }
  
  //private

  getBundledProducts(basket) {
    const products = basket.getAll()
    const productNames = Object.keys(products)

    return productNames.reduce((result, name) => {
      if (!!this.priceChart[name]) result.push(name)
      return result
    }, [])
  }
}

module.exports = { SinglePriceRule, BundledPriceRule }
