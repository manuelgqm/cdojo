const ProductsCollection = require('./ProductsCollection')
const { SinglePriceRule, BundledPriceRule } = require('./PriceRules')

class Checkout {
  constructor(customPriceRules) {
    this.basket = new ProductsCollection()
    this.priceRules = customPriceRules || this.defaultPriceRules() 
  }

  scan(productName) {
    this.basket.add(productName)

    return this
  }

  scanMany(productsNamesStream) {
    const separator = ""
    const productsNames = productsNamesStream.split(separator)
    
    productsNames.forEach((productName) => {
      this.scan(productName)
    })

    return this
  }

  printBasket() {
    return this.basket.print()
  }

  total() {
    let initial = {
      basket: this.basket,
      total: 0
    }

    let result = this.priceRules.reduce((acc, rule) => {
      const current = rule.apply(acc.basket)
      const basket = current.basket
      const total = current.total + acc.total 

      return { basket, total }
    }, initial)

    this.nextCustomer()

    return result.total
  }

  // private

  nextCustomer() {
    this.basket = new ProductsCollection()
  }

  defaultPriceRules() {
    return [new BundledPriceRule(), new SinglePriceRule()]
  }
}

module.exports = Checkout
