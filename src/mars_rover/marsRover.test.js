const MarsRover = require('./MarsRover')

describe('A Mars Rover', () => {
  it('kowns its landing position', () => {
    const landingPosition = '5,5,N'
    const rover = new MarsRover()
    rover.land(landingPosition)

    expect(rover.printCurrentPosition()).toEqual(landingPosition)
  })

  it('lands in different position', () => {
    const landingPosition = '6,6,N'
    const rover = new MarsRover()
    rover.land(landingPosition)

    expect(rover.printCurrentPosition()).toEqual(landingPosition)
  })

  describe('when performing a command', () => {
    let rover 

    beforeEach(() => {
      const landingPosition = '5,5,N'
      rover = new MarsRover()
      rover.land(landingPosition)
    })

    describe('moves', () => {
      it('forward once', () => {
        const finalPosition = '5,6,N'
        const command = 'F'

        rover.run(command)

        expect(rover.printCurrentPosition()).toEqual(finalPosition)
      })

      it('forward three times', () => {
        const finalPosition = '5,8,N'
        const command = 'FFF'

        rover.run(command)

        expect(rover.printCurrentPosition()).toEqual(finalPosition)
      })

      it('backwards twice', () => {
        const finalPosition = '5,3,N'
        const command = 'BB'

        rover.run(command)

        expect(rover.printCurrentPosition()).toEqual(finalPosition)
      })
    })

    describe('turns', () => {
      it('left once', () => {
        const finalPosition = '5,5,W'
        const command = 'L'

        rover.run(command)

        expect(rover.printCurrentPosition()).toEqual(finalPosition)
      })

      it('left twice', () => {
        const finalPosition = '5,5,S'
        const command = 'LL'

        rover.run(command)

        expect(rover.printCurrentPosition()).toEqual(finalPosition)
      })

      it('right once', () => {
        const finalPosition = '5,5,E'
        const command = 'R'

        rover.run(command)

        expect(rover.printCurrentPosition()).toEqual(finalPosition)
      })
    })

    describe('moves and turns', () => {
      it('everywhere', () => {
        const command = 'FFLBRFFR'
        const finalPosition = '6,9,E'

        rover.run(command)

        expect(rover.printCurrentPosition()).toEqual(finalPosition)
      })
    })
  })
})
