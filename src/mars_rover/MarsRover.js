const MoveCommand = require('./MoveCommand.js')
const TurnCommand = require('./TurnCommand.js')

class MarsRover {
  constructor () {
    this.currentPosition = { x: 0, y: 0, orientation: '' }
    this.currentOrientation = ''
    this.move = new MoveCommand(this.currentPosition)
    this.turn = new TurnCommand(this.currentPosition)
  }

  land (rawPosition) {
    const position = rawPosition.split(',')

    this.currentPosition.y = parseInt(position[0])
    this.currentPosition.x = parseInt(position[1])
    this.currentPosition.orientation = position[2]
  }

  printCurrentPosition () {
    const { y, x, orientation } = this.currentPosition
    return `${x},${y},${orientation}`
  }

  _execute (command) {
    const commandsMap = {
      'F': () => this.move.forward(),
      'B': () => this.move.backwards(),
      'L': () => this.turn.left(),
      'R': () => this.turn.right()
    }

    // const prevPosition = this.printCurrentPosition()
    commandsMap[command]();
    // console.log(`${prevPosition} -> ${command} -> ${this.printCurrentPosition()}`)
  }

  run (rawCommands) {
    const commands = rawCommands.split('')
    
    commands.forEach((command) => {
      this._execute(command)
    })
  }
}

module.exports = MarsRover
