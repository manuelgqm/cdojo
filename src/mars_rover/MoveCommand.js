const MOVEMENT_STEP = 1

class MoveCommand {
  constructor (position) {
    this.position = position
  }

  forward () {
    const movementDetail = this._getMovementDetail()
    this.move(movementDetail)
  }

  backwards () {
    const movesBackwards = true
    const movementDetail = this._getMovementDetail(movesBackwards)
    this.move(movementDetail)
  }

  _getMovementDetail (movesBackwards = false) {
    const { orientation } = this.position
    const xAxisDirections = 'EW'
    const forwardDirections = 'N,E'

    const axis = (xAxisDirections.includes(orientation)) ? 'x' : 'y'
    const backwadModifier = (movesBackwards) ? -MOVEMENT_STEP : MOVEMENT_STEP
    const amount = (forwardDirections.includes(orientation)) ? MOVEMENT_STEP * backwadModifier : -MOVEMENT_STEP * backwadModifier

    return { axis, amount }
  }

  move (detail) {
    this.position[detail.axis] += detail.amount
  }
}

module.exports = MoveCommand
