const SEQUENCE_STEP = 1
const DIRECTIONS_SEQUENCE = ['N', 'E', 'S', 'W']

class TurnCommand {
  constructor (position) {
    this.position = position
  }

  left () { this._turn(-SEQUENCE_STEP) }

  right () { this._turn(SEQUENCE_STEP) }

  _turn (sequenceStep) {
    const newSequenceIndex = this._getCurrentSequenceIndex() + sequenceStep
    let nextDirectionIndex = this._rotateIndex(this._getMaxSequenceIndex(), newSequenceIndex)

    this.position.orientation = DIRECTIONS_SEQUENCE[nextDirectionIndex]
  }

  _getCurrentSequenceIndex () {
    const { orientation } = this.position
    return DIRECTIONS_SEQUENCE.indexOf(orientation)
  }

  _getMaxSequenceIndex () {
    return DIRECTIONS_SEQUENCE.length - 1
  }

  _rotateIndex(maxIndex, index) {
    if (0 <= index && index <= maxIndex) return index

    let newIndex = 0
    if (index === -1) newIndex = maxIndex
      
    return newIndex
  }
}

module.exports = TurnCommand
