const IA = require("./IA.js")

describe("IA", () => {
  it('greets unknown people', () => {
    let result = IA.greet() 

    expect(result).toEqual("Hello, my friend.")
  })

  it('greets one known person', () => {
    let name = "Bob"
    let result = IA.greet(name)

    expect(result).toEqual("Hello, Bob.")
  })

  it('greets another known person', () => {
    let name = "Charlie"
    let result = IA.greet(name)

    expect(result).toEqual("Hello, Charlie.")
  })

  it('greets shouting', () => {
    let name = "JERRY"
    let result = IA.greet(name)

    expect(result).toEqual("HELLO JERRY!")
  })
})
