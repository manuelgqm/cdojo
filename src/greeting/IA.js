class IA {
  static greet(name = "my friend") {
    let salute = IA.get_salute(name)
    let punctuation_sign = IA.get_punctuation_sign(name)

    return `${salute} ${name}${punctuation_sign}`
  }

  static get_salute(name) {
    if (IA.is_shouted(name)) {
      return "HELLO"
    }  

    return "Hello,"
  }

  static get_punctuation_sign(name) {
    if (IA.is_shouted(name)) {
      return "!"
    }

    return "."
  }

  static is_shouted(name) {
    return name == name.toUpperCase()
  }  
} 

module.exports = IA
