FROM node:14-alpine

COPY package.json yarn.lock /cdojo/

WORKDIR cdojo

RUN yarn install

COPY ./src src

CMD ["yarn", "test"]
