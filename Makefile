image_name = node14-alpine
container_name = cdojo

build:
	docker build -t ${image_name} .

recreate:
	docker build -t ${image_name} . --no-cache
	
shell: 
	docker run -it -v $(shell pwd):/cdojo --rm --name ${container_name} ${image_name} sh

test:
	docker run --rm --name ${container_name} ${image_name}
